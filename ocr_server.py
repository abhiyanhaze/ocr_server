import logging
from flask import Flask, request, json
from ocr_fast import abbyy_python
import json
from cpp_ocr import CPPOCR


app = Flask(__name__)

logging.basicConfig(filename='ocr_server.log',level=logging.DEBUG)
logger = logging.getLogger(__name__)

@app.route("/get_text", methods=["POST"])
def get_text():
    response_json = {}
    ocr_data = request.form
    ocr_settings = json.loads(ocr_data["ocr_settings"])
    try:
        if not ocr_settings:
            logger.warning("OCR settings are not provided")
            raise Exception("OCR Settings are not provided")
        if (
            "ocr_function" in ocr_data.keys()
            and "image_file" in request.files
        ):
            uploaded_image = request.files.get("image_file")
            uploaded_image.seek(0)
            ocr_function = ocr_data.get('ocr_function')
            cpp_ocr = CPPOCR(uploaded_image.read(), ocr_settings=ocr_settings)
            if hasattr(cpp_ocr, ocr_function):
                func = getattr(cpp_ocr, ocr_function)
                extracted_text = func()
                response_json["ocr_text"] = extracted_text
                response_json["success"] = True
                response_json["status"] = 200
                response_json["error_message"] = ""
                logger.info("OCR request processed sucessfully")
            else:
                response_json["ocr_text"] = None
                response_json["success"] = False
                response_json["status"] = 402
                response_json["error_message"] = f"The given function {ocr_function} is not valid"
                logger.exception("Invalid ocr function %s given.", ocr_function)
        else:
            response_json["success"] = False
            response_json["error_message"] = "Required parameters missing"
            response_json["ocr_text"] = None
            response_json["status"] = 402
            logger.exception("Invalid arguements given %s", ocr_data)
    except Exception as e:
        response_json["success"] = False
        response_json["error_message"] = str(e)
        response_json["ocr_text"] = None
        response_json["status"] = 500
        # logger.exception("OCR has failed for requested function %s with error %s",ocr_function, str(e))

    response = app.response_class(
        response=json.dumps(response_json),
        status=response_json["status"],
        mimetype="application/json"
    )
    print(response_json)
    return response

if __name__ == '__main__':
    logger.info("App is starting...")
    app.run(debug=True)
    logger.info("App is started...")